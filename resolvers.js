import {ObjectId} from "promised-mongo";

const { query } = require('nact');

const resource = "precedent" || "competence";

module.exports = {

    Mutation:{
        changeCompetence: async (obj, args, ctx, info) => {

            const collectionItemActor = ctx.children.get("item");
			
			if (args.input.children_competences_ids) {
				args.input.children_competences_ids = args.input.children_competences_ids.map(e => new ObjectId(e));
			}
			
            if(args._id){
                return await query(collectionItemActor,  {"type": "competence", search:{_id: args._id}, input: args.input }, global.actor_timeout);
            }else{
                return await query(collectionItemActor,  {"type": "competence", input: args.input }, global.actor_timeout);
            }

        },

        changePrecedent: async (obj, args, ctx, info) => {

            const collectionItemActor = ctx.children.get("item");

            for (var k in args.input.competences_ids) {
                args.input.competences_ids[k] = new ObjectId(args.input.competences_ids[k]);
            }

            args.input.subject_id = new ObjectId(args.input.subject_id);
            if (args.input.project_id) {
				args.input.project_id = new ObjectId(args.input.project_id);
            }
            if(args._id){
                return await query(collectionItemActor,  {"type": "precedent", search: {_id: args._id}, input: args.input}, global.actor_timeout);
            }else{
                return await query(collectionItemActor,  {"type": "precedent", input: args.input }, global.actor_timeout);
            }

        }
    },
    Query: {

        getCompetence: async (obj, args, ctx, info) => {

            const collectionItemActor = ctx.children.get("item");

            return await query(collectionItemActor, {"type": "competence", search: {_id: args.id}}, global.actor_timeout);

        },

        getCompetences: async (obj, args, ctx, info) => {

            const collectionActor = ctx.children.get("collection")

            return await query(collectionActor, {"type": "competence"}, global.actor_timeout);

        },


        getPrecedent: async (obj, args, ctx, info) => {

            return (await ctx.db.precedent.aggregate(
                [
					{$match: {
						_id: new ObjectId(args.id)
					}},
                    {"$sort": {"date": -1}},
                    {
                        "$lookup": {
                            "from": "user",
                            "localField": "subject_id",
                            "foreignField": "_id",
                            "as": "subject"
                        }
                    },
                    {"$unwind": "$subject"},
                    {
                        "$lookup": {
                            "from": "competence",
                            "localField": "competences_ids",
                            "foreignField": "_id",
                            "as": "competences"
                        }
                    }
                ]))[0];

        },
    
        getPrecedents: async (obj, args, ctx, info) => {

            return await ctx.db.precedent.aggregate(
                [
                    {"$sort": {"date": -1}},
                    {
                        "$lookup": {
                            "from": "user",
                            "localField": "subject_id",
                            "foreignField": "_id",
                            "as": "subject"
                        }
                    },
                    {"$unwind": "$subject"},
                    {
                        "$lookup": {
                            "from": "competence",
                            "localField": "competences_ids",
                            "foreignField": "_id",
                            "as": "competences"
                        }
                    }
                ]);

        },

    },
	Competence : {
		children_competences : async (obj, args, context, info) => {
			const collectionActor = context.children.get("collection")

			if (obj.children_competences_ids) {
				return await query(collectionActor, {"type": "competence", "search": {_id: {$in : obj.children_competences_ids}}}, global.actor_timeout);
			}
			
		},
    },
    Precedent: {
        subject : async (obj, args, context, info) => {
            if (obj.subject)
            {
                obj.subject.full_name = obj.subject.first_name + " " + obj.subject.second_name
            }
            return obj.subject;
        }
    }
}